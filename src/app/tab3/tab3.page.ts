import { Component } from '@angular/core';
import { NavController, } from '@ionic/angular';
import { ResultatsPage } from './resultats/resultats.page';
import { Router } from '@angular/router';



@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  public myDate: String

  constructor(public navCtrl: NavController) 
  {
    this.myDate = new Date().toISOString();
  }

}

