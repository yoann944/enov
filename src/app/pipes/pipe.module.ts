import { NgModule } from '@angular/core';
import { Safe } from './safe.pipe';

@NgModule({
    declarations: [
        Safe,
    ],
    imports: [

    ],
    exports: [
        Safe,
    ]
    ,
})
export class PipesModule {}