import { Component } from '@angular/core';

import { Twitch } from '../services/twitch/twitch.service';
import { ChannelPage } from '../channel/channel.page'
import { NavCtrlService } from '../services/navCtrl/nav-ctrl.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  datas: any = [];
  pagination: number = 0;

  constructor(public navCtrl: Router, private twitch: Twitch, private navService : NavCtrlService) {
    twitch.streams(this.pagination).then(data => {
      this.datas = data.streams;
    });
  }

  async openChannel(DataChannel){
    // this.navCtrl.push(Channel, {
    //   DataChannel: DataChannel
    // });
    this.navService.myParam = DataChannel;
    await this.navCtrl.navigateByUrl('/channel')
    

  }

  infiniteScroll(ev){
    this.pagination =+ 25;
    this.twitch.streams(this.pagination).then(data => {
      for(let i of data.streams){
        this.datas.push(i);
      }
      ev.complete();
    });
  };

  doRefresh(ev){
    this.pagination = 0;
    this.twitch.streams(this.pagination).then(data => {
      ev.complete();
      this.datas = data.streams;
    })
  }
}
