import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/toPromise';

/*
  Generated class for the Twitch provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Twitch {
  
  client_id:any = '&client_id=j878m7f9jq7crl32m6ga23qd7vpbq9';
  constructor(public http: Http) {
  }

  streams(pagination){
    return this.http.get('https://api.twitch.tv/kraken/streams?offset='+pagination+this.client_id)
    .toPromise()
    .then(data => data.json());
  }

}
