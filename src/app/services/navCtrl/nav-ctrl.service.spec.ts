import { TestBed } from '@angular/core/testing';

import { NavCtrlService } from './nav-ctrl.service';

describe('NavCtrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavCtrlService = TestBed.get(NavCtrlService);
    expect(service).toBeTruthy();
  });
});
