import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(private http: HttpClient){ }

  public getAllStats() : Promise<any>
  {
    return new Promise(resolve => 
    {
      this.http.get('http/127.0.0.1/stats')
    })
  }

  public createStats(stats) : Observable<any>
  {
    return this.http.post('http/127.0.0.1/stat', JSON.stringify(stats))
  }

}
