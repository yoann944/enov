import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player8-stats',
  templateUrl: './player8-stats.page.html',
  styleUrls: ['./player8-stats.page.scss'],
})
export class Player8StatsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public async backTabtwo()
  {
    await this.router.navigateByUrl('/')
  }
}
