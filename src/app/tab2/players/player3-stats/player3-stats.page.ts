import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player3-stats',
  templateUrl: './player3-stats.page.html',
  styleUrls: ['./player3-stats.page.scss'],
})
export class Player3StatsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public async backTabtwo()
  {
    await this.router.navigateByUrl('/')
  }
}
