import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Player3StatsPage } from './player3-stats.page';

describe('Player3StatsPage', () => {
  let component: Player3StatsPage;
  let fixture: ComponentFixture<Player3StatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Player3StatsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Player3StatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
