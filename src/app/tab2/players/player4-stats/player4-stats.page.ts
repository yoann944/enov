import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player4-stats',
  templateUrl: './player4-stats.page.html',
  styleUrls: ['./player4-stats.page.scss'],
})
export class Player4StatsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public async backTabtwo()
  {
    await this.router.navigateByUrl('/')
  }

}
