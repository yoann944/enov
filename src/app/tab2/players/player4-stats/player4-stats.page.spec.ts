import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Player4StatsPage } from './player4-stats.page';

describe('Player4StatsPage', () => {
  let component: Player4StatsPage;
  let fixture: ComponentFixture<Player4StatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Player4StatsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Player4StatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
