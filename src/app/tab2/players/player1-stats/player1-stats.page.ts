import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player1-stats',
  templateUrl: './player1-stats.page.html',
  styleUrls: ['./player1-stats.page.scss'],
})
export class Player1StatsPage implements OnInit {

  constructor(private router: Router) { 

  }

  ngOnInit() {

  }
  public async backTabtwo()
  {
    await this.router.navigateByUrl('/')
  }

}
