import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Player1StatsPage } from './player1-stats.page';

describe('Player1StatsPage', () => {
  let component: Player1StatsPage;
  let fixture: ComponentFixture<Player1StatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Player1StatsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Player1StatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
