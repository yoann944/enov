import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Player9StatsPage } from './player9-stats.page';

describe('Player9StatsPage', () => {
  let component: Player9StatsPage;
  let fixture: ComponentFixture<Player9StatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Player9StatsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Player9StatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
