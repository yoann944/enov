import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player9-stats',
  templateUrl: './player9-stats.page.html',
  styleUrls: ['./player9-stats.page.scss'],
})
export class Player9StatsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public async backTabtwo()
  {
    await this.router.navigateByUrl('/')
  }
}
