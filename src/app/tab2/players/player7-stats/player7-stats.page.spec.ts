import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Player7StatsPage } from './player7-stats.page';

describe('Player7StatsPage', () => {
  let component: Player7StatsPage;
  let fixture: ComponentFixture<Player7StatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Player7StatsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Player7StatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
