import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player7-stats',
  templateUrl: './player7-stats.page.html',
  styleUrls: ['./player7-stats.page.scss'],
})
export class Player7StatsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public async backTabtwo()
  {
    await this.router.navigateByUrl('/')
  }
}
