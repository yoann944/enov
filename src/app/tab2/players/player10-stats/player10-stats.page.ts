import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player10-stats',
  templateUrl: './player10-stats.page.html',
  styleUrls: ['./player10-stats.page.scss'],
})
export class Player10StatsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public async backTabtwo()
  {
    await this.router.navigateByUrl('/')
  }
}
