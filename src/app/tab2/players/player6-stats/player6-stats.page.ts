import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player6-stats',
  templateUrl: './player6-stats.page.html',
  styleUrls: ['./player6-stats.page.scss'],
})
export class Player6StatsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public async backTabtwo()
  {
    await this.router.navigateByUrl('/')
  }

}
