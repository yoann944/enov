import { Component } from '@angular/core';
import { NavCtrlService } from '../services/navCtrl/nav-ctrl.service';
import { Router } from '@angular/router';

/*
  Generated class for the Channel page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-channel',
  templateUrl: 'channel.page.html',
  styleUrls: ['channel.page.scss']
})
export class ChannelPage  {

  data:any;
  dataTwitch: any;

  constructor(private navService: NavCtrlService, private router: Router) {
    //this.data = params.data.DataChannel;
  }

  ionViewDidLoad() {
    console.log('Hello Channel Page'); 
  }

  ngOnInit() {
    // this.dataTwitch = this.navService.myParam;
    this.data = this.navService.myParam;
  }

  public async backPage()
  {
    await this.router.navigateByUrl('/')
  }
}
