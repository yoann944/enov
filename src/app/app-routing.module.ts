import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'player1-stats', loadChildren: './tab2/players/player1-stats/player1-stats.module#Player1StatsPageModule' },
  { path: 'player2-stats', loadChildren: './tab2/players/player2-stats/player2-stats.module#Player2StatsPageModule' },
  { path: 'player3-stats', loadChildren: './tab2/players/player3-stats/player3-stats.module#Player3StatsPageModule' },
  { path: 'player4-stats', loadChildren: './tab2/players/player4-stats/player4-stats.module#Player4StatsPageModule' },
  { path: 'player5-stats', loadChildren: './tab2/players/player5-stats/player5-stats.module#Player5StatsPageModule' },
  { path: 'resultats', loadChildren: './tab3/resultats/resultats.module#ResultatsPageModule' },
  { path: 'tab2', loadChildren: './tab2/tab2.module#Tab2PageModule'},
  { path: 'tab3', loadChildren: './tab3/tab3.module#Tab3PageModule'},
  { path: 'channel', loadChildren: './channel/channel.module#ChannelPageModule' },  { path: 'player6-stats', loadChildren: './tab2/players/player6-stats/player6-stats.module#Player6StatsPageModule' },
  { path: 'player7-stats', loadChildren: './tab2/players/player7-stats/player7-stats.module#Player7StatsPageModule' },
  { path: 'player8-stats', loadChildren: './tab2/players/player8-stats/player8-stats.module#Player8StatsPageModule' },
  { path: 'player9-stats', loadChildren: './tab2/players/player9-stats/player9-stats.module#Player9StatsPageModule' },
  { path: 'player10-stats', loadChildren: './tab2/players/player10-stats/player10-stats.module#Player10StatsPageModule' }



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

